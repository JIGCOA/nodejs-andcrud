const express = require("express"); //framework ของ NodeJs
const server = express();
const ejs = require("ejs"); //View Source จักการ template HTML
const bcrypt = require("bcrypt"); // การเข้า รหัส
const mysql = require("mysql"); //จักการกับ ฐานข้อมูล
const cookieParser = require("cookie-parser"); // จักการกับcookie
const readCookie = cookieParser();
const session = require("express-session");

const bodyParser = require("body-parser");
const readBody = bodyParser.urlencoded({ extended: false });

//set conect database
const db = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "nodejscrud"
});

// connect to database
db.connect(err => {
  if (err) {
    throw err;
  }
  console.log("Connected to database");
});
global.db = db;

const port = server.listen(8080); // set port server เซ็ทค่า port server แสดงผลออก
server.set("port", process.env.port || port);
server.set("views", __dirname + "/views");
server.engine("html", ejs.renderFile);
//get
server.get("/", showHome);
server.get("/index", showHome);
server.get("/register", registershow);
server.get("/login", showlogin);
//post
server.post("/register", readBody, registeruser);
server.post("/login", readBody, function checkLogIn(req, res) {
  let userid = req.body.userid || "";
  let password = req.body.password || "";

  db.query("SELECT * FROM users WHERE 	user_name = ?", [userid], function(
    error,
    results,
    fields
  ) {
    if (results[0].password) {
      bcrypt.compare(password, results[0].password, function(err, result) {
        if (result) {
          res.render("complete.html");
        } else {
          res.render("error.html");
        }
      });
    }
  });
});

//functionในการทำงาน

function showHome(req, res) {
  res.render("index.html");
}
function showlogin(req, res) {
  res.render("login.html");
}

function registershow(req, res) {
  res.render("register.html");
}
function registeruser(req, res) {
  const saltRounds = 10000;

  let fname = req.body.fname || "";
  let lname = req.body.lname || "";
  let phone = req.body.phone || "";
  let userid = req.body.userid || "";
  let password = req.body.password || "";
  let pswrepeat = req.body.pswrepeat || "";
  let image = "";

  if (
    fname == "" ||
    lname == "" ||
    phone == "" ||
    userid == "" ||
    password == ""
  ) {
    res.redirect(req.url + "?message=Invalid Data");
    return;
  }
  if (password !== pswrepeat) {
    res.redirect(req.url + "?password ไม่ตรงกัน");
    return;
  }
  let hash = bcrypt.hashSync(password, saltRounds); //เข้าหรัสbcryptในpassword

  let data = [fname, lname, phone, image, userid, hash];
  const sql =
    "insert into users(first_name,last_name,phone,image,user_name,password) values" +
    "(?, ?, ?, ?, ?, ?)";
  db.query(sql, data, function(error, result) {
    if (error == null) {
      res.render("complete.html");
    } else {
      res.redirect(req.url + "?Rrror");
    }
  });
}
